using .MPI

"""
    CommInfo

Alias for `NamedTuple{(:comm, :group, :rank, :size), Tuple{MPI.Comm, MPI.Comm, Int, Int}}`
"""
struct CommInfo{B} 
    comm::MPI.Comm
    group::MPI.Group
    rank::Int
    size::Int
end

"""
    OptComms

Composite type holding the different communicators for MPI-based optimization.

# Fields:
- world::CommInfo  # The top-level communicator between all processors. Rank 0 on this communicator is the global head.
- inter_group::CommInfo  # Communicator linking all the group heads.  Rank 0 on this communicator is the global head.
- compute::CommInfo  # Communicator linking all the processors in a particular group.  Rank 0 on this communicator is the group head.
- group_id::Int
- is_world_head::Bool
- is_compute_head::Bool
"""
struct OptComms
    world::CommInfo
    inter_group::CommInfo
    compute::CommInfo
    group_id::Int
    is_world_head::Bool
    is_compute_head::Bool
    external_rank::Int
end

Base.show(io::IO, x::OptComms) =
    print(io, "OptComms: world =   ", x.world, "\n",
              "          group =   ", x.inter_group, "\n",
              "          compute = ", x.compute, "\n",
              "          group_id: ", x.group_id, ", is_world_head: ", x.is_world_head,
              ", is_compute_head: ", x.is_compute_head, ", external_rank: ", x.external_rank, "\n")

"""
    setup_mpi_comms(procs_per_group, procs_per_node, world_comm::MPI.Comm)

Split `world_comm` into groups where the communicator splitting attempts to
adhere as close to `procs_per_group` as possible, returning an `OptComms` type.

See also: [`OptComms`](@ref)
"""
function setup_mpi_comms(procs_per_group::Int,
                         procs_per_node::Int,
                         incoming_comm::MPI.Comm)
    incoming_size = MPI.Comm_size(incoming_comm)
    incoming_rank = MPI.Comm_rank(incoming_comm)
    incoming_group = MPI.Comm_group(incoming_comm)

    # Partition the workers into different groups ensuring that 
    # so that the groups don't overlap nodes
    n_procs_group, n_groups_per_node = partition_node(procs_per_node, procs_per_group)
    group_start_procs = cumsum(n_procs_group) .- n_procs_group .+ 1
    group_end_procs = circshift(group_start_procs, -1) .- 1
    group_end_procs[end] = group_end_procs[end - 1] + n_procs_group[end]
    n_nodes = div(incoming_size, procs_per_node)
    n_worker_groups = n_nodes * n_groups_per_node

    groups = Vector{MPI.Group}(undef, n_worker_groups)
    group_ranks = Vector{Vector{Int32}}(undef, n_worker_groups)
    compute_comms = Vector{MPI.Comm}(undef, n_worker_groups)
    
    for r in 1:n_groups_per_node
        group_ranks[r] = Int32.(collect(group_start_procs[r]:group_end_procs[r]))
    end
    for n in 1:n_nodes-1
        for r in 1:n_groups_per_node
            group_ranks[n_groups_per_node * n + r] = Int32.(collect(group_start_procs[r]:group_end_procs[r]) .+ procs_per_node * n)
        end
    end

    workers = Vector{Int32}(undef, 0)
    for i in 1:n_worker_groups
        workers = vcat(workers, group_ranks[i])
    end
    
    group_id = 0
    for (i, r) in enumerate(group_ranks)
        group_id = iszero(group_id) ? first(r) <= incoming_rank <= last(r) ? i : 0 : group_id 
    end

    prepend!(workers, zero(Int32))
    if incoming_rank in workers
        is_computable = true
        for i in 1:n_worker_groups
            groups[i] = MPI.Group_incl(incoming_group, group_ranks[i])
            compute_comms[i] = MPI.Comm_create_group(incoming_comm, groups[i], i)
        end

        if group_id != 0
            compute_comm = compute_comms[group_id]
            compute_group = groups[group_id]
            compute_rank = MPI.Comm_rank(compute_comm)
            compute_size = MPI.Comm_size(compute_comm)
        else
            compute_comm = MPI.COMM_NULL
            compute_group = MPI.GROUP_NULL
            compute_rank = 0
            compute_size = 1
        end

        world_group = MPI.Group_incl(incoming_group, workers)
        world_comm = MPI.Comm_create_group(incoming_comm, world_group, 0)
        world_rank = MPI.Comm_rank(world_comm)
        world_size = MPI.Comm_size(world_comm)
            
        head_ranks = prepend!(first.(group_ranks), zero(Int32))
        head_group = MPI.Group_incl(incoming_group, head_ranks)
        group_comm = MPI.Comm_create_group(incoming_comm, head_group, n_worker_groups * n_worker_groups)
        is_compute_head = incoming_rank in head_ranks
        group_rank = is_compute_head ? MPI.Comm_rank(group_comm) : -1
        group_size = is_compute_head ? MPI.Comm_size(group_comm) : -1
    else
        compute_comm = MPI.COMM_NULL
        compute_group = MPI.GROUP_NULL
        compute_rank = -1
        compute_size = -1
        group_comm = MPI.COMM_NULL
        head_group = MPI.GROUP_NULL
        group_rank = -1
        group_size = -1
        group_id = -1
        world_group = MPI.GROUP_NULL
        world_comm = MPI.COMM_NULL
        world_rank = -1
        world_size = -1
        is_compute_head = false
        is_computable = false
    end
    

    @debug OptComms(CommInfo{is_computable}(world_comm, world_group, world_rank, world_size),
                    CommInfo{is_computable}(group_comm, head_group, group_rank, group_size),
                    CommInfo{is_computable}(compute_comm, compute_group, compute_rank, compute_size),
                    group_id, world_rank == 0, is_compute_head, incoming_rank)
    return OptComms(CommInfo{is_computable}(world_comm, world_group, world_rank, world_size),
                    CommInfo{is_computable}(group_comm, head_group, group_rank, group_size),
                    CommInfo{is_computable}(compute_comm, compute_group, compute_rank, compute_size),
                    group_id, world_rank == 0, is_compute_head, incoming_rank)
end

"""
    partition_node(procs_per_node, procs_per_group)

Attempt to find a partitioning of on-node resources closest
to `procs_per_group` while keeping the first processor reserved
from doing calculation work.
"""
function partition_node(procs_per_node::Int,
                        procs_per_group::Int;
                       )
    # The 1st proc on every node is not a worker
    n_groups, rem_procs = divrem(procs_per_node - 1, procs_per_group)

    if rem_procs < floor(0.75 * procs_per_group)
        rem_procs_split, rem_procs_rem = divrem(rem_procs, n_groups)
        n_procs_group = push!(fill(procs_per_group + rem_procs_split, n_groups - 1),
                              procs_per_group + rem_procs_split + rem_procs_rem)
    else
        n_procs_group = push!(fill(procs_per_group, n_groups), rem_procs)
        n_groups += 1
    end
    return n_procs_group, n_groups
end

@inline function is_computable(::CommInfo{B}) where {B}
    return B
end


"""
    compute_all_targets!(prob::Problem, comm_info::CommInfo; kwargs...)

Compute all the targets in `prob` (possibly) using `comm_info` for the MPI
communication, updating the current value of each target in `prob`.  
The keyword arguments for computing targets are forwarded to appropriate
target method.

See also: [`compute_all_targets`](@ref)
"""
function compute_all_targets!(prob::Problem,
                              comms::OptComms;
                              kwargs...
                             )
    if comms.compute.rank == 0
        target_values = compute_all_targets(prob, comms; kwargs...)
        foreach(p -> prob.targets[p.first].current_value = p.second, target_values)
    else
        compute_all_targets(prob, comms; kwargs...)
    end
end

"""
    compute_all_targets(prob::Problem, comm::OptComms; kwards...)

Compute all the targets in `prob` (possibly) using `comms` for the MPI
communication, returning a dictionary of targets with updated current values.  
The keyword arguments for computing targets are forwarded to appropriate
target method.

See also: [`compute_all_targets!`](@ref)
"""
function compute_all_targets(prob::Problem,
                             comms::OptComms;
                             kwargs...
                            )
    return compute_targets(prob.targets, prob.equilibrium_wrappers, comms; kwargs...)
end

"""
    compute_targets(targets::Dict{Int, Target},
                    equilibrium_wrappers::Union{EquilibriumWrapper, Dict{UInt, EquilibriumWrapper}},
                    comms::OptComms;
                   )
    compute_targets(targets::Dict{Int, Target},
                    geometry::AbstractGeometry,
                    comms::OptComms;
                   )

Compute the targets associated with each equilibrium in `equilibrium_wrappers`
or geometry object `geometry` using `comms` for the MPI communication.
"""
function compute_targets(targets::Dict{Int, Target},
                         equilibrium_wrappers::Union{EquilibriumWrapper, Dict{UInt, EquilibriumWrapper}},
                         comms::OptComms;
                        )
                        
    if comms.compute.rank == 0
        T = typeof(first(targets).second.target_value)
        target_values =Dict{Int, T}()
        wrappers = equilibrium_wrappers isa Dict ? equilibrium_wrappers : Dict(one(UInt) => equilibrium_wrappers)
        for eq_wrapper in values(wrappers)
            for key in keys(eq_wrapper.derived_geometries)
                geometry = extract_derived_geometry(eq_wrapper, key)
                geometry_targets = filter(t -> t.second.geometry_key == key, targets)
                MPI.bcast(false, comms.compute.comm; root = 0)
                #MPI.bcast(key, 0, comms.compute)
                MPI.bcast(geometry, comms.compute.comm; root = 0)
                MPI.bcast(geometry_targets, comms.compute.comm; root = 0)
                #@debug "Rank $rank on comm $comm is calling targets with geometry $key"
                results = compute_targets(deepcopy(geometry_targets), geometry, comms)
                foreach(p -> target_values[p.first] = p.second, results)
            end
        end
        MPI.bcast(true, comms.compute.comm; root = 0)

        return target_values
    else
        finished = false
        while !finished
            finished = MPI.bcast(nothing, comms.compute.comm, root = 0)
            if !finished
                #key = MPI.bcast(nothing, 0, comms.compute)
                geometry = MPI.bcast(nothing, comms.compute.comm; root = 0)
                geometry_targets = MPI.bcast(nothing, comms.compute.comm; root = 0)
                #@debug "Rank $rank on comm $comm is calling targets with geometry $key"
                compute_targets(geometry_targets, geometry, comms)

            end
        end
    end
end
        

function compute_targets(targets::Dict{Int, Target},
                         geometry::E,
                         comms::OptComms;
                        ) where {E <: AbstractGeometry}

    if comms.compute.rank == 0
        T = typeof(first(targets).second.target_value)
        target_values = Dict{Int, T}()
        for (i, target) in targets
            MPI.bcast(false, comms.compute.comm; root = 0)
            MPI.bcast(target, comms.compute.comm; root = 0)
            #@debug "Rank $(comms.compute.rank) in group $(comms.group_id) with world rank $(comms.external_rank) is calling $(target.label)"
            target_values[i] = compute_target(target, geometry, comms)
        end
        MPI.bcast(true, comms.compute.comm; root = 0)

        return target_values
    else
        finished = false
        while !finished
            finished = MPI.bcast(nothing, comms.compute.comm; root = 0)
            if !finished
                target = MPI.bcast(nothing, comms.compute.comm; root = 0)
                #@debug "Rank $(comms.compute.rank) in group $(comms.group_id) with world rank $(comms.external_rank) is calling $(target.label)"
                compute_target(target, geometry, comms)

            end
        end
    end
end

"""
    compute_target(target::Target,
                   geometry::AbstractGeometry,
                   comms::OptComms;
                  )
Compute the target with geometry information from `geometry` using 
the communicator in `comms` for the MPI communication.
"""
function compute_target(target::Target,
                        geometry::E,
                        comms::OptComms;
                       ) where {E <: AbstractGeometry}
    if comms.compute.rank == 0
        has_comm = :mpi_comm in target.method.args
        MPI.bcast(has_comm, comms.compute.comm; root = 0)
        target_method = Expr(target.method.head, 
                             replace(target.method.args, 
                                     target.geometry_key => geometry,
                                     :mpi_comm => comms.compute.comm)...)
        MPI.bcast(target_method, comms.compute.comm; root = 0)
        return eval(target_method)
    else
        has_comm = MPI.bcast(nothing, comms.compute.comm; root = 0)
        target_method = MPI.bcast(nothing, comms.compute.comm; root = 0)
        if has_comm
            eval(target_method)
        end
    end
end
