using .CairoMakie

function plot_target_histories(history::OptimizationOutput{T, I},
                               targets::Union{S, Vector{S}};
                               resolution = (500, 300),
                               fontsize = 30,
                              ) where {S <: AbstractString, T <: Real, I}

    n_targets = length(targets)
    iter_count = length(history)
    target_histories = Array{T}(undef, iter_count, n_targets)
    for i in 1:n_targets
        target_index = findfirst(t -> t == targets[i], history.target_ids)
        target_histories[:, i] .= history.targets[:, target_index]
    end

    colors = [:black, :red, :blue, :magenta, :orange, :green]

    fig = Figure(resolution = resolution, fontsize=fontsize, font = "new computer modern roman")
    set_theme!(Theme(font = "new computer modern roman", fontsize = fontsize))
    gl = fig[1, 1] = GridLayout()
    axs = [Axis(gl[n, 1], limits = ((0, iter_count-1), (extrema(target_histories[:, n]))),
           xlabel = "Iteration", xlabelsize = fontsize, ylabel = targets[n], ylabelsize = fontsize) for n in 1:n_targets]

    for ax in axs[1:end-1]
        hidexdecorations!(ax, grid = false, minorgrid = false)
        hideydecorations!(ax, ticks = false, ticklabels = false, label = false)
    end
    hideydecorations!(axs[end], ticks = false, ticklabels = false, label = false)
    rowgap!(gl, 10)

    for t in 1:n_targets
        #lines!(axs[t], 0:iter_count-1, fill(0.5*sum(extrema(target_histories[:, t])), iter_count), linewidth = 2, color = :black)
        scatterlines!(axs[t], 0:iter_count-1, target_histories[:, t], color = colors[t], linewidth = 2, linestyle=:dot, markersize = 5)
    end

    return fig
end
