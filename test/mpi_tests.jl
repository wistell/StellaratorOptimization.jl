import Pkg
Pkg.activate("/home/bfaber/projects/julia/lasso")
Pkg.instantiate()

using PlasmaEquilibriumToolkit
using VMEC
using lasso
using MPI
#using GammaC

ENV["JULIA_DEBUG"] = lasso

MPI.Init()
ppg = 4
comms = lasso.setupComms(ppg,MPI.COMM_WORLD)
rank = MPI.Comm_rank(comms.globalComm)
prob = lasso.Problem()
if rank == 0
  lasso.readInputFile(prob,"/home/bfaber/projects/julia/lasso/test/qa_input.txt")
  println("There are ",length(prob.variables)," optimization variables")
  sv = Vector{Float64}(undef,length(prob.variables))
  rv = Dict(v=>k for (k,v) in lasso.VmecOpt.translator)
  for (k,v) in prob.variables
    sv[rv[k]] = v.value
  end
  prob.stateVector = sv
end
prob = MPI.bcast(prob,0,comms.globalComm)
lasso.VmecOpt.broadcast(comms.globalComm)
MPI.Barrier(comms.globalComm)
#=
if rank != 0
  if MPI.Comm_rank(comms.evalComm) == 0
    result = lasso.computeObjective(sv,lassoInfo,comms)
    MPI.Send([result],0,0,comms.groupComm)
  else
    lasso.computeObjective(sv,lassoInfo,comms)
  end
else
  result, status = MPI.Recv(Float64,1,0,comms.groupComm)
  println(result)
end
=#

# Tests whether the objective gradient calculation works
#=
if rank != 0
  lasso.computeObjectiveGradient(prob.stateVector,prob,comms)
else
  F, G = lasso.computeObjectiveGradient(prob.stateVector,prob,comms)
  println("F: ",F)
  println("G:")
  display(G)
  println('\n')
end
=#

residual = lasso.optimizeMPI(prob,comms)

if rank == 0
  show(residual)
end
