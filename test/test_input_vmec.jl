using MPI, Optim, LineSearches, JLD2
using StellaratorOptimization, StellaratorOptimizationMetrics, VMEC
ENV["JULIA_DEBUG"] = StellaratorOptimization

if !MPI.Initialized()
  MPI.Init()
end
VMEC.load_vmec_lib("libvmec_mkl.so")
# Adjust the number in this call for a different number of processors
# per group of workers
comms = StellOpt.setup_mpi_comms(4, 128, MPI.COMM_WORLD)

optimizer = StellOpt.Optimizer(GradientDescent(; alphaguess=1e+0, linesearch=Static()),
                               Optim.Options(iterations = 2, allow_f_increases = true))
prob = StellOpt.Problem()
prob.output_directory = @__DIR__
prob.output_tag = "test"
StellOpt.add_optimizer!(prob, optimizer)

fd = StellOpt.CenteredDiff(1e-5)
spsa = StellOpt.SPSA(α = 0.602, β = 1_000, γ = 0.101, δ = 1e-4, ϵ = 1e-3, n = 32)
nml = VMEC.read_vmec_namelist(joinpath(@__DIR__, "input.aten"))

vmec_wrapper = StellOpt.EquilibriumWrapper(nml, Vmec, spsa)
StellOpt.add_equilibrium_wrapper!(prob, vmec_wrapper)

vmec_s05 = StellOpt.@add_derived_geometry!(prob, VmecSurface(0.5, equilibrium=vmec_wrapper))

StellOpt.@add_target!(prob, VMEC.quasisymmetry_deviation(1, 4, 16, 16, vmec_s05), 0.0, 1.0, "qs")
ζ_range = 0.0:2π/128:100π
B_resolution = 100
StellOpt.@add_mpi_target!(prob,
                          StellOptMetrics.GammaC.gamma_c_target(vmec_s05, first(ζ_range), step(ζ_range), last(ζ_range),
                                                                B_resolution, comms, false, true), 0.0, 1.0, "Γc(s = 0.5)")
StellOpt.@add_target!(prob, VMEC.magnetic_well(vmec_s05), -0.1, 1.0, "well")

StellOpt.add_multiple_modes!(prob, vmec_wrapper, -2, 2; max_m = 2)

#tg = Dict{Int, Vector{Float64}}()
#for key in keys(prob.targets)
#    tg[key] = Vector{Float64}(undef, length(prob.state_vector))
#end
#StellOpt.objective_gradient!(0.0, zeros(Float64, length(prob.state_vector)), first(prob.equilibrium_wrappers).second, prob.state_vector, prob.targets, tg, comms)
#StellOpt.gradient(first(prob.equilibrium_wrappers).second, prob.state_vector, prob.targets, comms)
res = StellOpt.optimize_mpi(prob, comms)

