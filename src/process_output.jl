struct OptimizationOutput{T, I}
    dofs::I
    n_targets::I
    target_ids::Array{String, 1}
    state_vector_ids::Array{String, 1}
    gradient::Array{T, 2}
    jacobian::Array{T, 3}
    targets::Array{T, 2}
    values::Array{T, 2}
    weights::Array{T, 2}
    state_vector::Array{T, 2}
    objective::Array{T}
end

function n_targets(output::OptimizationOutput{T, I}) where {T, I}
    return output.n_targets
end

function dofs(output::OptimizationOutput{T, I}) where {T, I}
    return output.dofs
end

function target_ids(output::OptimizationOutput{T, I}) where {T, I}
    return output.target_ids
end

function state_vector_ids(output::OptimizationOutput{T, I}) where {T, I}
    return output.state_vector_ids
end

function Base.length(output::OptimizationOutput{T, I}) where {T, I}
    return length(output.objective)
end

function Base.:+(a::OptimizationOutput{T, I},
                 b;
                ) where {T, I}
    return a
end

function Base.:+(a,
                 b::OptimizationOutput{T, I};
                ) where {T, I}
    return b
end

function Base.:+(a::OptimizationOutput{T, I},
                 b::OptimizationOutput{T, I};
                ) where {T, I}
    c_length = length(a) + length(b)
    n_target = n_targets(a) == n_targets(b) ? n_targets(a) : throw(DimensionMismatch("Number of targets in output are incompatible ($(n_targets(a)) vs. $(n_targets(b)))"))
    n_dofs = dofs(a) == dofs(b) ? dofs(a) :
        throw(DimensionMismatch("Number of dofs in output are incompatible ($(dofs(a)) vs. $(dofs(b)))"))
    
    targets = if target_ids(a) ⊆ target_ids(b) 
        if target_ids(a) == target_ids(b)
            target_ids(a)
        else
            printstyled("Warning: target id orders do not match", bold=true, color=Base.warn_color())
            target_ids(a)
        end
    else
        throw(ArgumentError("The target ids do not match"))
    end

    state_vars = if state_vector_ids(a) ⊆ state_vector_ids(b) 
        if state_vector_ids(a) == state_vector_ids(b)
            state_vector_ids(a)
        else
            printstyled("Warning: state vector id orders do not match", bold=true, color=Base.warn_color())
            state_vector_ids(a)
        end
    else
        throw(ArgumentError("The state vector ids do not match"))
    end

    c_grad = Array{T, 2}(undef, c_length, n_dofs)
    c_tar = Array{T, 2}(undef, c_length, n_target)
    c_wgt = similar(c_tar)
    c_sv = similar(c_grad)
    c_jac = Array{T, 3}(undef, c_length, n_target, n_dofs)
    c_obj = Array{T, 1}(undef, c_length)

    c_grad[1:length(a), :] .= copy(a.gradient)
    c_grad[length(a)+1:end, :] .= copy(b.gradient)
    c_tar[1:length(a), :] .= copy(a.targets)
    c_tar[length(a)+1:end, :] .= copy(b.targets)
    c_wgt[1:length(a), :] .= copy(a.weights)
    c_wgt[length(a)+1:end, :] .= copy(b.weights)
    c_sv[1:length(a), :] .= copy(a.state_vector)
    c_sv[length(a)+1:end, :] .= copy(b.state_vector)
    c_jac[1:length(a), :, :] .= copy(a.jacobian)
    c_jac[length(a)+1:end, :, :] .= copy(b.jacobian)
    c_obj[1:length(a)] .= a.objective
    c_obj[length(a)+1:end] .= b.objective

    return OptimizationOutput(n_dofs, n_target, targets, state_vars,
                              c_grad, c_jac, c_tar, c_wgt, c_sv, c_obj)
end
    

function read_history_h5(filename::S) where {S <: AbstractString}

    file_id = h5open(filename)
    dof = read_attribute(file_id, "degrees of freedom")
    n_targets = read_attribute(file_id, "number of targets")
    target_ids = read_attribute(file_id, "target ids")
    state_vector_ids = read_attribute(file_id, "state vector ids")
    jac_size = read_attribute(file_id, "jacobian size")
    
    #count how many groups there are
    ngroup = 0
    for group_id in file_id
        ngroup += 1
    end

    if ngroup == 0
        println("No iterations of data in file")
        return
    end

    #get the type
    T = typeof(file_id["1"]["objective"]["gradient"][1])
    gradient = Array{T}(undef, ngroup, dof)
    jacobian = Array{T}(undef, ngroup, jac_size[1], jac_size[2])
    targets = Array{T}(undef, ngroup, n_targets)
    values = similar(targets)
    weights = similar(targets)
    state_vector = similar(gradient)
    objective = Array{T}(undef, ngroup)
    

    for iter_id in keys(file_id)
        i = parse(Int, iter_id) 
        state_vector[i,:] = read(file_id[iter_id]["state vector"])
        objective[i] = read(file_id[iter_id]["objective"]["value"])
        gradient[i,:] = read(file_id[iter_id]["objective"]["gradient"])
        for (tid, target) in enumerate(target_ids)
            values[i, tid] = read(file_id[iter_id][target]["value"])
            targets[i,tid] = read(file_id[iter_id][target]["target"])
            weights[i, tid] = read(file_id[iter_id][target]["weight"])
            jacobian[i, tid, :] = read(file_id[iter_id][target]["gradient"])
        end
    end
       
    close(file_id)

    return OptimizationOutput(dof, n_targets, target_ids, state_vector_ids, 
                                gradient, jacobian, targets, values, weights,
                                state_vector, objective)
end

function read_history_h5(filenames::AbstractVector{S}) where {S <: AbstractString}
    output = nothing
    for file in filenames
        output += read_history_h5(file)
    end
    return output
end
