
function objective_gradient!(F,
                             G,
                             wrapper::EquilibriumWrapper{T, E, D},
                             state_vector::AbstractVector{OptimizationVariable},
                             targets::Dict{Int, Target},
                             target_gradients::Dict{Int, Vector{T}},
                             comms::OptComms; 
                            ) where {T, E <: AbstractMagneticEquilibrium, D <: SPSA}
    translate!(wrapper, state_vector)
    if comms.is_world_head
        obj, grad = (zero(T), nothing)
        if !isnothing(F) 
            if !iszero(wrapper.grad_method.f)
                MPI.bcast(false, comms.world.comm; root = 0)
                obj = wrapper.grad_method.f
                for key in keys(targets)
                    targets[key].current_value = wrapper.grad_method.targets[key]
                end
            else
                MPI.bcast(true, comms.world.comm; root = 0)
                obj = compute_objective(wrapper, state_vector, targets, comms)
                wrapper.grad_method.f = obj
                wrapper.grad_method.targets = Dict{Int, T}()
                for key in keys(targets)
                    wrapper.grad_method.targets[key] = zero(T)
                end
                wrapper.grad_method.∇f = zeros(T, length(state_vector))
            end
        else
            MPI.bcast(false, comms.world.comm; root = 0)
        end
        if !isnothing(G) && ((!isnothing(F) && !isnothing(obj)) || isnothing(F))
            MPI.bcast(true, comms.world.comm; root = 0)
            old_obj = copy(wrapper.grad_method.f)
            target_derivatives = Dict{Int, Target}()
            grad, target_derivatives = gradient(wrapper, deepcopy(state_vector), targets, comms)
            copyto!(G, grad)
            new_obj = copy(wrapper.grad_method.f) 
            println("Comparing new objective $new_obj to $old_obj")
            for key in keys(target_derivatives)
                target_gradients[key] = copy(target_derivatives[key])
            end
        else
            MPI.bcast(false, comms.world.comm; root = 0)
        end
        return obj
    else
        objective_needed = MPI.bcast(nothing, comms.world.comm; root = 0)
        if objective_needed
            compute_objective(wrapper, state_vector, targets, comms)
        end
        gradient_needed = MPI.bcast(nothing, comms.world.comm; root = 0)
        if gradient_needed
            gradient(wrapper, deepcopy(state_vector), targets, comms)
        end
        return nothing
    end
end

"""
    gradient(equilibrium_wrapper::EquilibriumWrapper{T, E, SPSA}, state_variables::Vector{OptimizationVariable}, targets::Dict{Int, Target}, comms::OptComms) where {T, E}

MPI parallelized routine for calculating the Simultaneous Perturbation Stochastic Approximation of the gradient.  The function also computes the next objective value to take advantage of available work.
""" 
function gradient(equilibrium_wrapper::EquilibriumWrapper{T, E, D},
                  state_variables::Vector{OptimizationVariable},
                  targets::Dict{Int, Target},
                  comms::OptComms;
                  kwargs...
                ) where {T, E <: AbstractMagneticEquilibrium, D <: SPSA}

    if comms.is_world_head
        local_g = deepcopy(equilibrium_wrapper.grad_method)
        n_samples = local_g.n
        group_size = comms.inter_group.size
        target_gradients = Dict{Int, Vector{T}}()
        for key in keys(targets)
            target_gradients[key] = Vector{T}(undef, length(state_variables))
        end
        gₖ = copy(local_g.∇f)
        group_status = fill((status = false, neighbor = 0), group_size-1)
        grad_status = fill((false, false), local_g.n)
         
        for group_id in 1:div(group_size, 2)
            if group_id <= local_g.n
                MPI.send(false, comms.inter_group.comm; dest = 2*group_id-1, tag = 0)
                MPI.send(2*group_id, comms.inter_group.comm; dest = 2*group_id-1, tag = 2*group_id-1)
                MPI.send(false, comms.inter_group.comm; dest = 2*group_id, tag = 0)
                MPI.send(2*group_id-1, comms.inter_group.comm; dest = 2*group_id, tag = 2*group_id)
                group_status[2*group_id-1] = (status = true, neighbor = 2*group_id)
                group_status[2*group_id] = (status = true, neighbor = 2*group_id-1) 
                grad_status[group_id] = (true, false)
            else
                MPI.send(true, comms.inter_group.comm; dest = 2*group_id-1, tag = 0)
                MPI.send(true, comms.inter_group.comm; dest = 2*group_id, tag = 0)
                group_status[2*group_id-1] = (status = false, neighbor = -1)
                group_status[2*group_id] = (status = false, neighbor = -1)
            end
        end
        while !all(s -> s[1] && s[2], grad_status)
            status = findall(p -> !p.status, group_status)
            if !isempty(status)
                start_index = findfirst(i -> !i[1] && !i[2], grad_status)
                if !isnothing(start_index)
                   if length(status) >= 2 
                        forward_group = status[1]
                        backward_group = status[2]
                        MPI.send(false, comms.inter_group.comm; dest = forward_group, tag = 0)           
                        MPI.send(backward_group, comms.inter_group.comm; dest = forward_group, tag = forward_group)
                        MPI.send(false, comms.inter_group.comm; dest = backward_group, tag = 0)
                        MPI.send(forward_group, comms.inter_group.comm; dest = backward_group, tag = backward_group)
                        group_status[status[1]] = (status = true, neighbor = backward_group)
                        group_status[status[2]] = (status = true, neighbor = forward_group)
                        grad_status[start_index] = (true, false)
                    else
                        MPI.send(true, comms.inter_group.comm; dest = status[1], tag = 0)
                        group_status[status[1]] = (status = false, neighbor = -1)
                    end
                end
            end
            
            index = MPI.recv(comms.inter_group.comm; source = MPI.ANY_SOURCE, tag = MPI.ANY_TAG)
            neighbor_index = MPI.recv(comms.inter_group.comm; source = index, tag = 4 * group_size + index)
            valid_eq = MPI.recv(comms.inter_group.comm; source = index, tag = index)
            insert_index = findfirst(i -> i[1] && !i[2], grad_status)
            if valid_eq
                new_g = MPI.recv(comms.inter_group.comm; source = index, tag = group_size + index)
                new_gₖ = MPI.recv(comms.inter_group.comm; source = index, tag = 2 * group_size + index)
                new_target_gradients = MPI.recv(comms.inter_group.comm; source = index, tag = 3 * group_size + index)
                if new_g.f < local_g.f || insert_index == 1
                    copyto!(gₖ, new_gₖ)
                    for key in keys(targets)
                        copyto!(target_gradients[key], new_target_gradients[key])
                        local_g.targets[key] = new_g.targets[key] 
                    end
                    local_g.f = copy(new_g.f)
                    copyto!(local_g.∇f, new_g.∇f)
                    local_g.α = copy(new_g.α)
                    local_g.k = copy(new_g.k)
                end
                grad_status[insert_index] = (true, true)
            else
                grad_status[insert_index] = (false, false)
            end
            group_status[index] = (status = false, neighbor = 0)
            group_status[neighbor_index] = (status = false, neighbor = 0)
        end
        status = findall(p -> !p.status && iszero(p.neighbor), group_status)
        if !isnothing(status)
            for s in status
                MPI.send(true, comms.inter_group.comm; dest = s, tag = 0)
                group_status[s] = (status = false, neighbor = -1)
            end
        end
        all(p -> !p.status && p.neighbor == -1, group_status) || error("Fatal error in SPSA gradient, MPI ranks have not returned") 
        MPI.bcast(local_g, comms.inter_group.comm; root = 0)
        equilibrium_wrapper.grad_method.f = copy(local_g.f)
        equilibrium_wrapper.grad_method.k = local_g.k
        equilibrium_wrapper.grad_method.α = local_g.α
        copyto!(equilibrium_wrapper.grad_method.∇f, local_g.∇f)
        for key in keys(targets)
            equilibrium_wrapper.grad_method.targets[key] = local_g.targets[key]
        end
        @debug "Final target_gradients: $target_gradients"
        @debug "aₖ * gₖ = $(gₖ)"
        return gₖ, target_gradients
    else
        simultaneous_perturbation(equilibrium_wrapper, state_variables, targets, comms)
        if comms.is_compute_head
            new_g = MPI.bcast(nothing, comms.inter_group.comm; root = 0)
            MPI.bcast(new_g, comms.compute.comm; root = 0)
        else
            new_g = MPI.bcast(nothing, comms.compute.comm; root = 0)
        end
        equilibrium_wrapper.grad_method.k = new_g.k
        equilibrium_wrapper.grad_method.α = new_g.α
        copyto!(equilibrium_wrapper.grad_method.∇f, new_g.∇f)
        equilibrium_wrapper.grad_method.f = new_g.f
        for key in keys(targets)
            equilibrium_wrapper.grad_method.targets[key] = new_g.targets[key]
        end
        return nothing
    end

end

"""
    simultaneous_perturbation(equilibrium_wrapper::EquilibriumWrapper{T, E, SPSA}, state_variables::AbstractVector{OptimizationVariable}, targets::Dict{Int, Target}, comms::OptComms) where {T, E}

MPI-parallelized routine for computing the simulatneous perturbation gradient approximation and 
the test steps for the next iteration using two worker groups.
"""
function simultaneous_perturbation(equilibrium_wrapper::EquilibriumWrapper{T, E, D},
                                   state_variables::AbstractVector{OptimizationVariable},
                                   targets::Dict{Int, Target},
                                   comms::OptComms;
                                  ) where {T, E <: AbstractMagneticEquilibrium, D <: SPSA}
    rank = comms.compute.rank
    original_input = deepcopy(equilibrium_wrapper.input)
    eq = nothing
    θ = map(v -> v.value, state_variables)
    δθ = similar(θ)
    g = equilibrium_wrapper.grad_method
    g.∇f = zeros(T, length(state_variables))

    if comms.is_compute_head
        Δ = similar(θ)
        cₖ = g.δ/(g.k^g.γ)
        gₖ = zeros(T, length(θ))
        group_id = comms.inter_group.rank
        group_size = comms.inter_group.size
        target_values = Dict{Int, T}()
        target_gradients = Dict{Int, Vector{T}}()
        for key in keys(targets)
            target_gradients[key] = Vector{T}(undef, length(state_variables))
        end
        finished = MPI.recv(comms.inter_group.comm; source = 0, tag = 0)

@debug "Group $(comms.inter_group.rank) has received $finished"

        MPI.bcast(finished, comms.compute.comm; root = 0)

        while !finished
            gₖ = zeros(T, length(θ))
            neighbor = MPI.recv(comms.inter_group.comm; source = 0, tag = group_id) 
            if group_id < neighbor
                Δ .= rand(Bernoulli(), length(state_variables)) |> x -> cₖ * (2x .- 1)
                MPI.send(Δ, comms.inter_group.comm; dest = neighbor, tag = group_id)
@debug "$(comms.group_id) sent Δ to $neighbor"
                δθ = θ + Δ
            else
                Δ = MPI.recv(comms.inter_group.comm; source = neighbor, tag = neighbor)
@debug "$(comms.group_id) received Δ from $neighbor"
                δθ = θ - Δ
            end
            
            MPI.bcast(δθ, comms.compute.comm; root = 0)
            foreach(p -> state_variables[p[1]].value = p[2], enumerate(δθ))
            translate!(equilibrium_wrapper, state_variables)
            eq = compute_equilibrium(equilibrium_wrapper, comms.compute.comm)
            if group_id < neighbor
                MPI.send(isnothing(eq), comms.inter_group.comm; dest = neighbor, tag = 1)
                neighbor_eq = MPI.recv(comms.inter_group.comm; source = neighbor, tag = 2)
            else
                neighbor_eq = MPI.recv(comms.inter_group.comm; source = neighbor, tag = 1)
                MPI.send(isnothing(eq), comms.inter_group.comm; dest = neighbor, tag = 2)

            end
            
            if !isnothing(eq) && !neighbor_eq
                MPI.bcast(true, comms.compute.comm; root = 0)
                update_equilibrium!(equilibrium_wrapper, eq)
                push!(target_values, compute_targets(targets, equilibrium_wrapper, comms)...)
                if group_id < neighbor
                    back_targets = MPI.recv(comms.inter_group.comm; source = neighbor, tag = 3)
                    for key in keys(targets)
                        forward_objective = (targets[key].target_value - target_values[key])^2/targets[key].target_weight^2
                        back_objective = (targets[key].target_value - back_targets[key])^2/targets[key].target_weight^2
                        target_gradients[key][:] .= 0.5 * (target_values[key] - back_targets[key]) ./ Δ
                        @debug comms.inter_group.rank,  0.5 * (forward_objective - back_objective) ./ Δ
                        gₖ[:] .+= 0.5 * (forward_objective - back_objective) ./ Δ
                        @debug comms.inter_group.rank, gₖ
                    end
                    αₖ = dot(gₖ, g.∇f) < zero(T) ? g.α + 1 : g.α
                    qₖ = min(one(T), αₖ / g.k + 0.5)
                    aₖ = g.ϵ/((g.β + g.k)^qₖ)
                    MPI.send(gₖ, comms.inter_group.comm; dest = neighbor, tag = 4)
                    MPI.send(aₖ, comms.inter_group.comm; dest = neighbor, tag = 5)
                    aₖ *= 1.5
                    xₖ = θ .- aₖ .* gₖ
                    MPI.bcast(xₖ, comms.compute.comm; root = 0)
                    set_input!(equilibrium_wrapper, original_input) 
                    foreach(p -> state_variables[p[1]].value = p[2], enumerate(xₖ))
                    translate!(equilibrium_wrapper, state_variables)
                    new_eq = compute_equilibrium(equilibrium_wrapper, comms.compute.comm)
                    if !isnothing(new_eq)
                        update_equilibrium!(equilibrium_wrapper, new_eq)
                        new_targets = empty(target_values) 
                        push!(new_targets, compute_targets(targets, equilibrium_wrapper, comms)...)
                        new_objective = zero(T)
                        for key in keys(targets)
                            new_objective += (targets[key].target_value - new_targets[key])^2/targets[key].target_weight^2
                        end
                    end
                     
                    neighbor_targets = MPI.recv(comms.inter_group.comm; source = neighbor, tag = 6)
                    if !isnothing(neighbor_targets)
                        neighbor_objective = zero(T)
                       for key in keys(targets)
                           neighbor_objective += (targets[key].target_value - neighbor_targets[key])^2/targets[key].target_weight^2
                       end
                    else
                        neighbor_objective = 100 * g.f 
                    end
                    if !isnothing(new_eq)
                       if neighbor_objective < new_objective
                            obj = neighbor_objective
                            aₖ = 0.5 * g.ϵ/((g.β + g.k)^qₖ)
                            g.targets = copy(neighbor_targets)
                        else
                            obj = new_objective
                            g.targets = copy(new_targets)
                        end
                    else
                        obj = neighbor_objective
                        aₖ = 0.5 * g.ϵ/((g.β + g.k)^qₖ)
                        if !isnothing(neighbor_targets)
                            g.targets = copy(neighbor_targets)
                        end
                    end
                    g.k = g.k + 1
                    g.α = αₖ
                    g.∇f = copy(aₖ .* gₖ)
                    g.f = obj
                    MPI.send(group_id, comms.inter_group.comm; dest = 0, tag = 0)
                    MPI.send(neighbor, comms.inter_group.comm; dest = 0, tag = 4 * group_size + group_id)
                    MPI.send(true, comms.inter_group.comm; dest = 0, tag = group_id)
                    MPI.send(g, comms.inter_group.comm; dest = 0, tag = group_size + group_id)
                    MPI.send(aₖ .* gₖ, comms.inter_group.comm; dest = 0, tag = 2 * group_size + group_id)
                    MPI.send(target_gradients, comms.inter_group.comm; dest = 0, tag = 3 * group_size + group_id)
                else
                    MPI.send(target_values, comms.inter_group.comm; dest = neighbor, tag = 3)
                    gₖ = MPI.recv(comms.inter_group.comm; source = neighbor, tag = 4)
                    aₖ = MPI.recv(comms.inter_group.comm; source = neighbor, tag = 5)
                    aₖ *= 0.5
                    xₖ = θ .- aₖ .* gₖ
                    MPI.bcast(xₖ, comms.compute.comm; root = 0)
                    set_input!(equilibrium_wrapper, original_input)
                    foreach(p -> state_variables[p[1]].value = p[2], enumerate(xₖ))
                    translate!(equilibrium_wrapper, state_variables)
                    eq = compute_equilibrium(equilibrium_wrapper, comms.compute.comm)
                    if !isnothing(eq)
                        update_equilibrium!(equilibrium_wrapper, eq)
                        new_targets = empty(target_values) 
                        push!(new_targets, compute_targets(targets, equilibrium_wrapper, comms)...)
                        MPI.send(new_targets, comms.inter_group.comm; dest = neighbor, tag = 6)
                    else
                        MPI.send(nothing, comms.inter_group.comm; dest = neighbor, tag = 6)
                    end
                end
            else
                MPI.bcast(false, comms.compute.comm; root = 0)
                MPI.send(group_id, comms.inter_group.comm; dest = 0, tag = group_id)
                MPI.send(neighbor, comms.inter_group.comm; dest = 0, tag = 4 * group_size + group_id)
                MPI.send(false, comms.inter_group.comm; dest = 0, tag = group_size + group_id) 
            end

            set_input!(equilibrium_wrapper, original_input)
            foreach(p -> state_variables[p[1]].value = p[2], enumerate(θ))
            finished = MPI.recv(comms.inter_group.comm; source = 0, tag = 0)
@debug "Group $(comms.inter_group.rank) has received $finished"
            MPI.bcast(finished, comms.compute.comm; root = 0)
        end
    else
        finished = MPI.bcast(nothing, comms.compute.comm; root = 0)
        while !finished
            δθ = MPI.bcast(nothing, comms.compute.comm; root = 0)
            foreach(p -> state_variables[p[1]].value = p[2], enumerate(δθ))
            translate!(equilibrium_wrapper, state_variables)
            eq = compute_equilibrium(equilibrium_wrapper, comms.compute.comm)
            no_skip = MPI.bcast(nothing, comms.compute.comm; root = 0)
            if !isnothing(eq) && no_skip
                update_equilibrium!(equilibrium_wrapper, eq)
                compute_targets(targets, equilibrium_wrapper, comms)
                set_input!(equilibrium_wrapper, original_input)

                xₖ = MPI.bcast(nothing, comms.compute.comm; root = 0) 
                foreach(p -> state_variables[p[1]].value = p[2], enumerate(xₖ))
                translate!(equilibrium_wrapper, state_variables)
                new_eq = compute_equilibrium(equilibrium_wrapper, comms.compute.comm)
                if !isnothing(new_eq)
                    update_equilibrium!(equilibrium_wrapper, new_eq)
                    compute_targets(targets, equilibrium_wrapper, comms)
                end
            end
            set_input!(equilibrium_wrapper, original_input)
            foreach(p -> state_variables[p[1]].value = p[2], enumerate(θ))
            finished = MPI.bcast(nothing, comms.compute.comm; root = 0)
        end
    end
end

