
"""
    @add_mpi_target!(prob, method, target, weight, label)

Construct a target from a method signature provided in `method.`  The
method signature *must* have an argument corresponding to an
identifier returned by `add_equilibrium_wrapper` or `@add_derived_geometry`.

Example:
```julia-repl
julia> using MPI, VMEC, StellaratorOptimization

julia> prob = StellOpt.Problem{Float64}();

julia> vmec_wrapper = EquilibriumWrapper(input_dictionary);

julia> add_equilibrium_wrapper!(prob, vmec_wrapper);

julia> vmec_surface = @add_derived_geometry!(prob, VmecSurface(0.5, equilibrium = vmec_wrapper));

julia> @add_mpi_target!(prob, VMEC.quasisymmetry_deviation(1, 4, 16, 16, vmec_surface), 0.0, 1.0, "qs(0.5)");
```
"""
macro add_mpi_target!(prob, method, target, weight, label)
  mod = @__MODULE__

  #method_call = Expr(method.head, method.args...)
  #sym, index = find_last_module(method)
  #method_call.args[1] = insert_main(method, sym, index)
  method_expr = Expr(:quote, method)
  return esc(quote
        # Need to evaluate an argument in the calling scope
        # so the values can be passed to the calling function
        # The first argument will always be the function to be called
        method_call = Expr($method_expr.head, $method_expr.args...)
        calling_mod, index = $mod._find_calling_module(method_call)
        escaped_mod = Symbol(@__MODULE__)
        method_call = $mod._insert_escaped_module(method_call, escaped_mod, calling_mod, index)
        for i in 2:length($method_expr.args)
            if Meta.isexpr($method_expr.args[i], :kw)
                method_call.args[i].args[2] = eval($method_expr.args[i].args[2])
            else
                method_call.args[i] = eval($method_expr.args[i])
            end
        end
        geometry_slot = findfirst(x -> x isa UInt, method_call.args)
        geometry_key = !isnothing(geometry_slot) ? method_call.args[geometry_slot] : error("Target must have a UInt identifier for the geometry")
        comm_slot = findfirst(x -> x isa $mod.OptComms, method_call.args)
        if !isnothing(comm_slot)
            method_call.args[comm_slot] = :mpi_comm
        end
        $mod.add_target!($prob, $mod.Target($target, $weight, method_call, geometry_key, $label))
  end)
  
end
