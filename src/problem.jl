

"""
    Optimizer

Composite type holding the method and options for an optimizer, concretized on instantiation of a Problem.
"""
struct Optimizer
    method
    options
end

"""
    StellOptIteration{T}

Composite type holding the values of the objective, gradient and jacobian at each iteration

# Fields:
- `iteration::Int`
- `state_vector::Vector{T}`
- `objective::T`
- `gradient::Vector{T}`
- `objective_terms::Vector{T}`
- `jacobian::Matrix{T}`  # Rows are the targets, columns are the optimization variables
"""
struct StellOptIteration{T}
    iteration::Int
    state_vector::Vector{T}
    objective::T
    gradient::Vector{T}
    objective_terms::Vector{T}
    jacobian::Matrix{T}
end

"""
    Problem{T}

Composite structure representing a lasso optimization problem.

# Fields:
- `targets::Dict{Int, Target}`  # Dictionary containing the optimization targets, indexed by an integer value
- `state_vector::Vector{OptimizationVariable}`  # Dictionary containing the optimization variables, indexed by variable symbol
- `optimizer::Optimizer`  # Optimization method
- `equilibrium_wrappers::Dict{UInt, EquilibriumWrapper}`  # Dictionary of `EquilibriumWrapper`s, one for each equilibrium type used in the optimization problem
- `derived_geometries::Dict{UInt, DerivedGeometryWrapper}` # Dictionary of `DerivedGeometryWrapper`s, one for each derived geometry object (like magnetic surface or fieldline) used in target calculations
- `objective::T` # An int to keep track of how many objective function evaluations have occurred 
- `gradien::Vector{T}` # An int to keep track of the number of gradient evaluations
- `output_tag::String` #A file name to write status output to
- `output_directory::String` #A file name to write gradient output to
"""
mutable struct Problem{T}
    targets::Dict{Int, Target}
    state_vector::Vector{OptimizationVariable}
    optimizer::Optimizer
    equilibrium_wrappers::Dict{UInt, EquilibriumWrapper}
    derived_geometries::Dict{UInt, DerivedGeometryWrapper}
    history::Vector{StellOptIteration{T}}
    iteration::Int
    objective::T
    gradient::Vector{T}
    output_tag::String
    output_directory::String
    write_input_interval::Int
    Problem() = Problem{Float64}()
    Problem{T}() where {T} = new{T}(Dict{Int, Target}(),
                                    Vector{OptimizationVariable}(undef, 0),
                                    Optimizer(nothing, nothing),
                                    Dict{UInt, EquilibriumWrapper}(),
                                    Dict{UInt, Expr}(), Vector{StellOptIteration{T}}(undef, 0),
                                    1, convert(T, Inf), Vector{T}(undef, 0), "opt_run", pwd(), 1)
    Problem{T}(a, b, c, d, e, f, g, h, i, j, k, l, m, n) where {T} = new(a, b, c, d, e, f, g, h, i, j, k, l, m, n)
end