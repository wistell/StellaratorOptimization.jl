
function open_history_file(prob::Problem; )
    name_stem = "stellopt_history_$(prob.output_tag)"
    filename = joinpath(prob.output_directory, name_stem*".h5")
    fid = h5open(filename, "w")
    
    attributes(fid)["degrees of freedom"] = length(prob.state_vector)
    attributes(fid)["number of targets"] = length(prob.targets)
    attributes(fid)["jacobian size"] = [length(prob.targets), length(prob.state_vector)]

    attributes(fid)["state vector ids"] = map(v -> name(v), prob.state_vector)
    attributes(fid)["target ids"] = [name(target) for (i, target) in prob.targets]
    fid
end

function write_history_record(fid::HDF5.File,
                              record::StellOptIteration,
                              count::Int;
                             )
    iter = record.iteration
    try
        create_group(fid, "$count")
    catch
    end
    gid = fid["$count"]
    
    id = "Iteration"
    write(gid, id, iter)

    id = "State vector"
    write(gid, id, record.state_vector)

    id = "Total objective"
    write(gid, id, record.objective)

    id = "Gradient"
    write(gid, id, record.gradient)

    id = "Objective terms"
    write(gid, id, record.objective_terms)

    id = "Jacobian"
    write(gid, id, record.jacobian)
    close(gid)
    return nothing
end

function write_history_record(prob::Problem{T},
                             ) where {T}

    name_stem = "stellopt_history_$(prob.output_tag)"
    filename = joinpath(prob.output_directory, name_stem*".h5")
    file_id = h5open(filename, "r+") 
    iter = prob.iteration
    try
        create_group(file_id, "$iter")
    catch
    end
    iter_id = file_id["$iter"]

    for t in values(prob.targets)
        write_target_record!(iter_id, t)
    end

    try
        delete_object(iter_id, "objective")
    catch
    end
    create_group(iter_id, "objective")
    obj_id = iter_id["objective"]

    id = "value"
    write(obj_id, id, prob.objective)

    id = "gradient"
    write(obj_id, id, prob.gradient)

    id = "state vector"
    sv = state_vector_values(prob)
    try
        write(iter_id, id, sv)
    catch
        delete_object(iter_id, id)
        write(iter_id, id, sv)
    end

    close(file_id)
    return nothing
end

function write_history_file(prob::Problem;)
    fid = open_history_file(prob)

    for (i, record) in enumerate(prob.history)
        write_history_record(fid, record, i)
    end
    close(fid)
end

function write_history_record(prob::Problem,
                              record::StellOptIteration,
                              count::Int;
                             )
    name_stem = "stellopt_history_$(prob.output_tag)"
    filename = joinpath(prob.output_directory, name_stem*".h5")
    fid = h5open(filename, "r+") 
    write_history_record(fid, record, count)
    close(fid)
    return nothing
end

function write_history_attributes(prob::Problem;)
    fid = open_history_file(prob)
    close(fid)
    return nothing
end

function write_target_record!(group_id::HDF5.Group,
                              target::Target{T};
                             ) where {T}
    try
        delete_object(group_id, name(target))
    catch
    end
    create_group(group_id, name(target))
    tid = group_id[name(target)]
    
    id = "value"
    write(tid, id, target.current_value)

    id = "target"
    write(tid, id, target.target_value)

    id = "weight"
    write(tid, id, target.target_weight)

    id = "gradient"
    write(tid, id, target.gradient)

    return nothing
end
