@testset "test output routines" begin
  output_filename = joinpath(@__DIR__, "stellopt_history_test_circular.h5")
  output_data = read_history_h5(output_filename)
  @testset "test output read" begin
    @test output_data.dofs ==2
    @test size(output_data.gradient) == (2,2)
    @test size(output_data.jacobian) == (2,2,2)
    @test output_data.n_targets == 2
    @test length(output_data.objective) == 2
    @test size(output_data.state_vector) == (2,2)
    @test length(output_data.state_vector_ids) == 2
    @test length(output_data.target_ids) == 2
    @test size(output_data.weights) == (2,2)
    @test size(output_data.targets) == (2,2)
  end
end
