Documentation for `StellaratorOptimization.jl`'s internals.

## Types
```@autodocs
Modules = [StellaratorOptimization]
Public = false
Pages = ["equiilibrium_wrapper.jl",
         "target.jl",
         "variables.jl",
         "problem.jl"]
```

## Macros/Utilities
```@autodocs
Modules = [StellaratorOptimization]
Public = false
Pages = ["macros.jl",
         "utilities.jl",
        ]
```

## Thread-based Optimization
```@autodocs
Modules = [StellaratorOptimization]
Public = false
Pages = ["optimize_threads.jl",
         "gradient.jl",
         "spsa.jl",
        ]
```
## MPI-based Optimization
```@autodocs
Modules = [StellaratorOptimization]
Public = false
Pages = ["macros_mpi.jl",
         "utilities_mpi.jl",
         "optimize_mpi.jl",
         "gradient_mpi.jl",
         "spsa_mpi.jl",
        ]
```

## Output
```@autodocs
Modules = [StellaratorOptimization]
Public = false
Pages = ["output.jl",
         "process_output.jl"
        ]
```
